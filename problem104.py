import collections
class Solution:
    def firstUniqChar(self, s: str) -> int:
        string = collections.Counter(s)
        result = []
        for char, count in string.items():
            if count == 1:
                result.append(char)
        
        if result == []:
            return -1
        
        return list(s).index(result[0])