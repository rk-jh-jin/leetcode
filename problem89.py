from collections import Counter
class Solution:
    def getHint(self, secret: str, guess: str) -> str:
        secretList = list(secret)
        guessList = list(guess)
        bullCount = 0
        i = 0
        while i < len(secretList):
            if secretList[i] == guessList[i]:
                bullCount += 1
                del secretList[i], guessList[i]
                i -= 1
            i += 1
        
        
                
        return str(bullCount) + "A" + str(sum((Counter(secretList)&Counter(guessList)).values())) + "B"

        # s, g, i = list(secret), list(guess), 0
        # while i < len(s):
        #     print(i)
        #     if g[i] == s[i]:
        #         del s[i], g[i]
        #         i -= 1
        #     i += 1
        # return str(len(secret)-len(s))+'A'+str(sum((Counter(s)&Counter(g)).values()))+'B'