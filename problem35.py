# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def hasCycle(self, head: ListNode) -> bool:
#         if head == None:
#             return False
#         else:
#             try:
#                 return self.checkCycle(head)
#             except RecursionError:
#                 return True
        
#     def checkCycle(self, head: ListNode):
#         if head.next == None:
#             return False
#         else:
#             return self.checkCycle(head.next)
        try:
            slow = head
            fast = head.next
            while slow is not fast:
                slow = slow.next
                fast = fast.next.next
            return True
        except:
            return False