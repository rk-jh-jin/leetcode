class Solution:
    def wordPattern(self, pattern: str, str: str) -> bool:
#         saveCase = {}
#         patternList = list(pattern)
#         string = list(str.split())
        
#         if len(set(patternList)) != len(set(string)) or len(patternList) != len(string):
#             return False
        
#         for i in range(len(patternList)):
#             if patternList[i] in saveCase:
#                 if saveCase[patternList[i]] != string[i]:
#                     return False
                
#             else:
#                 saveCase[patternList[i]] = string[i]
#         return True
    
        s = pattern
        t = str.split()
        return len(set(zip(s, t))) == len(set(s)) == len(set(t)) and len(s) == len(t)