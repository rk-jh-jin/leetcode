# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def isSymmetric(self, root: TreeNode) -> bool:
        return self.isMirror(root, root)
        
    def isMirror(self, root1: TreeNode, root2: TreeNode):
        if root1 == None and root2 == None:
            return True
        elif root1 == None or root2 == None:
            return False
        else:
            if root1.val == root2.val:
                if self.isMirror(root1.left, root2.right) is True:
                    if self.isMirror(root1.right, root2.left) is True:
                        return True
                    else:
                        return False
                else:
                    return False
            else:
                return False
            