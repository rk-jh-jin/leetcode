class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        for i in range(len(nums)):
            if target <= nums[i]:
                return i
            elif nums[-1] < target:
                return len(nums)

solution = Solution()
test = solution.searchInsert([1, 3, 5, 6], 5)
print(test)

