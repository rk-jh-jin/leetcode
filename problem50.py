class Solution:
    def rotate(self, nums: List[int], k: int) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
#         if k == 0:
#             return
        
#         number = nums[-1]
#         del nums[-1]
#         nums.insert(0, number)
#         k -= 1
        
#         return self.rotate(nums, k)
        savecase1 = nums[len(nums) - k:]
        savecase2 = nums[:len(nums) - k]
        nums[:] = savecase1 + savecase2