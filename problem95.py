class Solution:
    def reverseVowels(self, s: str) -> str:
        vowelList = ["a", "e", "i", "o", "u", "A", "E", "I", "O", "U"]
        stringList = list(s)
        left, right = 0, len(s) - 1
        while left < right:
            if stringList[left] in vowelList and stringList[right] in vowelList:
                stringList[left], stringList[right] = stringList[right], stringList[left]
                left, right = left + 1, right - 1
            
            elif stringList[left] in vowelList and stringList[right] not in vowelList:
                right -= 1
                
            elif stringList[left] not in vowelList and stringList[right] in vowelList:
                left += 1
                
            else:
                left, right = left + 1, right - 1
                
        return str("".join(stringList))