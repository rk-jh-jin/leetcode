class Solution:
    def merge(self, nums1: List[int], m: int, nums2: List[int], n: int) -> None:
        """
        Do not return anything, modify nums1 in-place instead.
        """
        index = 0
        for i in range(m, m + n):
            nums1[i] = nums2[index]
            index += 1
        if 0 in nums1 and m + n < len(nums1):
            zero_num = nums1.count(0)
            nums1.remove(0)
            nums1.sort()
            for i in range(zero_num):
                nums1.append(0)
        else:
            nums1.sort()