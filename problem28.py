# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    result = []
    def hasPathSum(self, root: TreeNode, sum: int) -> bool:
        self.result = []
        self.makeTreeSum(root, 0)
        print(self.result)
        if sum in self.result:
            return True
        else:
            return False
     
    def makeTreeSum(self, node: TreeNode, num: int):
        if not node:
            return
        else:
            num += node.val
            if not node.left and not node.right:
                self.result.append(num)
            
            if node.left:
                self.makeTreeSum(node.left, num)
            
            if node.right:
                self.makeTreeSum(node.right, num)
            

    def hasPathSum(self, root, sum):
        if not root:
            return False

        if not root.left and not root.right and root.val == sum:
            return True
        
        sum -= root.val

        return self.hasPathSum(root.left, sum) or self.hasPathSum(root.right, sum)