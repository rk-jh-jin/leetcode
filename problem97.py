class Solution:
    def intersection(self, nums1: List[int], nums2: List[int]) -> List[int]:
        i = 0
        result = []
        while i < len(nums1):
            if nums1[i] in nums2:
                result += [nums1[i]]
            i += 1
        return set(result)