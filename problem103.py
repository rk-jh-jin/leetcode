import collections
class Solution:
    def canConstruct(self, ransomNote: str, magazine: str) -> bool:
#         ransomList = list(ransomNote)
#         magazineList = list(magazine)
#         result = []
#         for i in range(len(ransomList)):
#             if ransomList[i] in magazineList:
#                 result.append(ransomList[i])
#                 magazineList.remove(ransomList[i])
        
#         if len(result) == len(ransomList):
#             return True
        
#         return False
        
        ransomList = collections.Counter(ransomNote)
        magazineList = collections.Counter(magazine)
        for char, count in ransomList.items():
            if magazineList[char] < count:
                return False
            
        return True