class Solution:
    def isPalindrome(self, x: int) -> bool:
        newx = list(str(abs(x)))
        newx.reverse()
        result = int("".join(newx))
        return result == x
        

solution = Solution()
result = solution.isPalindrome(121)
print(result)