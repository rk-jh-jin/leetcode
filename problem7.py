# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        if l1 == None:
            return l2
        elif l2 == None:
            return l1
        else:
            def saveList(node: ListNode):
                result = []
                result += [node.val]
                if node.next == None:
                    return result
                else:
                    temp = saveList(node.next)
                    result += temp
                    return result

            result = saveList(l1) + saveList(l2)
            result.sort()

            def LN(node: ListNode, index):
                if node.next == None:
                    node.next = ListNode(result[index])
                    index += 1
                    if index == len(result):
                        return
                    else:
                        LN(node.next, index)
                else:
                    LN(node.next, index)

            firstLN = ListNode(result[0])
            LN(firstLN, 1)
            return firstLN