import math
class Solution:
    def isPowerOfTwo(self, n: int) -> bool:
        if n == 1:
            return True
        elif n % 2 == 1 or n <= 0:
            return False
        
        num = math.log(n, 2)
        num2 = int(num)
        return abs(num - num2) <= 1e-9
    
        #return n > 0 and ((n & (n - 1)) == 0)