class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        # for i in range(len(nums)):
        #     if nums.count(nums[i]) == 1:
        #         return nums[i]
        # while len(nums) != 1:
        #     number = nums[0]
        #     if nums.count(number) != 1:
        #         for i in range(2):
        #             nums.remove(number)
        #     else:
        #         return nums[0]
        # return nums[0]
        # hash_table = {}
        # for i in nums:
        #     try:
        #         hash_table.pop(i)
        #     except:
        #         hash_table[i] = 1
        # return hash_table.popitem()[0]
        return sum(set(nums)) * 2 - sum(nums)