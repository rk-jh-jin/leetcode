# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def invertTree(self, root: TreeNode) -> TreeNode:
        result = None
        if not root:
            return root
        
        result = TreeNode(root.val)
        result.left = root.right
        result.right = root.left
        result.right = self.invertTree(root.left)
        result.left = self.invertTree(root.right)
        
        return result