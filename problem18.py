class Solution:
    def climbStairs(self, n: int) -> int:
        if n == 1:
            return 1
        elif n == 2:
            return 2
        else:
            first_num = 1
            second_num = 2
            for i in range(n - 2):
                 cur_num = first_num + second_num
                 first_num = second_num
                 second_num = cur_num
            return cur_num