class Solution:
    def missingNumber(self, nums: List[int]) -> int:
        # newList = set(range(len(nums) + 1))
        # result = list(newList - set(nums))
        # return result[0]
        
        n = len(nums)
        return int(n * (n + 1) / 2) - sum(nums)