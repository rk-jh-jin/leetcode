# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def minDepth(self, root: TreeNode) -> int:
        count = 0
        return self.checkDepth(root, count)
      
    def checkDepth(self, node: TreeNode, count):
        if not node:
            return count
        else:
            if not node.left and not node.right:
                count += 1
                return count
            elif not node.left and node.right:
                count += 1
                return self.checkDepth(node.right, count)
            elif not node.right and node.left:
                count += 1
                return self.checkDepth(node.left, count)
            else:
                count += 1
                return min(self.checkDepth(node.left, count), self.checkDepth(node.right, count))