class Solution:
    def maxProfit(self, prices: List[int]) -> int:
#         if not prices or len(prices) == 1:
#             return 0
        
#         min_price = max(prices)
#         max_profit = 0
#         for i in range(len(prices)):
#             if prices[i] < min_price:
#                 min_price = prices[i]
#             elif (prices[i] - min_price > max_profit):
#                 max_profit = prices[i] - min_price
        
#         return max_profit
        max_profit, min_price = 0, float('inf')
        for price in prices:
            min_price = min(min_price, price)
            profit = price - min_price
            max_profit = max(max_profit, profit)
        return max_profit