class Solution:
    def twoSum(self, numbers: List[int], target: int) -> List[int]:
        # StartPoint = 0
        # EndPoint = len(numbers) - 1
        # while StartPoint < EndPoint:
        #     result = numbers[StartPoint] + numbers[EndPoint]
        #     if result == target:
        #         return [StartPoint + 1, EndPoint + 1]
        #     elif result < target:
        #         StartPoint += 1
        #     else:
        #         EndPoint -= 1
        
        dic = {}
        for i, num in enumerate(numbers):
            if target-num in dic:
                return [dic[target-num]+1, i+1]
            dic[num] = i