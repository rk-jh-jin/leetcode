class Solution:
    def isValid(self, s: str) -> bool:
        case = list(s)
        testcase = []
        for i in range(len(case)):
            if case[i] == '(' or case[i] == '{' or case[i] == '[':
                testcase.append(case[i])
            elif case[i] == ')' or case[i] == '}' or case[i] == ']':
                test = testcase.pop() + case[i]
                if test != '()' and test != '{}' and test != '[]':
                    return False
        if len(testcase) != 0:
            return False
        else:
            return True

solution = Solution()
result = solution.isValid("()")
print(result)