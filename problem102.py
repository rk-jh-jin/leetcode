# The guess API is already defined for you.
# @param num, your guess
# @return -1 if my number is lower, 1 if my number is higher, otherwise return 0
# def guess(num: int) -> int:

class Solution:
    def guessNumber(self, n: int) -> int:
        maxVal = n
        minVal = 1
        num = (n - 1) // 2 + 1
        result = guess(num)
        while result != 0:
            if result == -1:
                maxVal = num - 1
            else:
                minVal = num + 1
            num = (maxVal - minVal) // 2 + minVal
            result = guess(num)
        return num