class Solution:
    import string
    def isPalindrome(self, s: str) -> bool:
        strData = []
        for i in s:
            if i in string.ascii_letters:
                t = i.lower()
                strData.append(t)
            elif i.isdigit():
                strData.append(i)
                
        if strData == []:
            return True
        # print(strData, len(strData))
        for i in range(len(strData) // 2 + 1):
            if strData[i] != strData[len(strData) - 1 - i]:
                return False
        
        return True