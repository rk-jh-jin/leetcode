class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        i = 0
        while i < len(nums):
            if nums[i] == nums[i - 1] and len(nums) != 1:
                del nums[i]
            else:
                i += 1
        return i