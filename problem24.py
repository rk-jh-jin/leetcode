# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

 #class Solution:
#     dictionary = []
    
#     def levelOrderBottom(self, root: TreeNode) -> List[List[int]]:
    #     self.dictionary = []
    #     self.makeDict(root, 0)
    #     return self.dictionary[::-1]
    
    
    # def makeDict(self, node: TreeNode, depth: int):
    #     if node == None:
    #         return
    #     try:
    #         if self.dictionary[depth] != None:
    #             self.dictionary[depth].append(node.val)
    #     except IndexError:
    #         self.dictionary.insert(depth, [node.val])
            
    #     if node.left != None:
    #         self.makeDict(node.left, depth + 1)
            
    #     if node.right != None:
    #         self.makeDict(node.right, depth + 1)

def levelOrderBottom(self, root):
    if not root:# #if root == None:
        return []
    level = [root] # [val: ~, [left: ~, right: ]]
    stack = []
    while level: #level != None
        stack.append([each.val for each in level]) #[[root.val]]
        level = [node for each in level for node in (each.left, each.right) if node]
        # TreeNode = {val: ~, left: ~, right: ~}
        # for each in level:
        #   for node in (each.left, each.right):
        #       if node = None:
        #       
        # if node != None: each.left, each.right의 each에 대해
        # level
    return stack[::-1]

[TreeNode{val: 9, left: None, right: None}, TreeNode{val: 20, left: TreeNode{val: 15, left: None, right: None}, right: TreeNode{val: 7, left: None, right: None}}]
[TreeNode{val: 15, left: None, right: None}, TreeNode{val: 7, left: None, right: None}]
[]
