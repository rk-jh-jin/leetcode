class Solution:
    def titleToNumber(self, s: str) -> int:
#         dic = {}
#         alpha = list(ascii_uppercase)
#         for i in range(len(alpha)):
#             dic[alpha[i]] = i + 1
            
#         stringList = list(s) #ABC
#         num = len(stringList) #3
#         result = 0
#         print(stringList)
#         for i in range(num): # 0 1 2
#             multi = num - i - 1
#             print(multi)
#             result += (26 ** multi) * dic[stringList[i]]
                
#         return result
    
        return reduce(lambda x,y:x*26+y,map(lambda x:ord(x)-ord('A')+1,s))