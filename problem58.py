class Solution:
    def isHappy(self, n: int) -> bool:
        saveNumber = []
        return self.checkHappy(n, saveNumber)
    
    def checkHappy(self, num, saveNumber):
        result = 0
        for i in str(num):
            result += (int(i) ** 2)
        
        if result == 1:
            return True
        
        if result in saveNumber:
            return False
        else:
            saveNumber.append(result)
            return self.checkHappy(result, saveNumber)