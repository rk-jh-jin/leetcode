class Solution:
    def convertToTitle(self, n: int) -> str:
        dic = {}
        for i, alpha in enumerate(list(ascii_uppercase)):
            dic[i] = alpha
        
        quo = (n - 1) // 26
        rem = (n - 1) % 26
        result = []
        while quo != 0:
            result.insert(0, dic[rem])
            rem = (quo - 1) % 26
            quo = (quo - 1) // 26
        result.insert(0, dic[rem])
        return "".join(result)