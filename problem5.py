class Solution:
    def longestCommonPrefix(self, strs: [str]) -> str:
        words = strs
        if len(words) < 1:
            return ""
        words_len = map(len, words) #[6, 4, 6]
        result = ""

        for i in range(1, min(words_len) + 1): #[1, 2, 3, 4]
            sample = words[0][:i] #f

            count = 0
            for j in range(1, len(words)):
                if sample == words[j][:i]:
                    count += 1

            if count == len(words) - 1:
                result = sample
            else: 
                break 

        return result