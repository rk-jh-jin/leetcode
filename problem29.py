class Solution:
    def generate(self, numRows: int) -> List[List[int]]:
        result = []
        if numRows == 0:
            return []
        else:
            for i in range(1, numRows + 1):
                trirow = [1] * i
                if i > 2:
                    for j in range(1, i - 1):
                        trirow[j] = savetri[j - 1] + savetri[j]

                savetri = trirow
                result.append(trirow)
            return result