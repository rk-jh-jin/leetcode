class Solution:
    def hammingWeight(self, n: int) -> int:
        oribin = list('{0:032b}'.format(n))
        return oribin.count("1")