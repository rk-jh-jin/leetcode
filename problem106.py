class Solution:
    def isSubsequence(self, s: str, t: str) -> bool:
        sList = list(s)
        tList = list(t)
        for i in sList:
            if i not in tList:
                return False
            
            tList = tList[tList.index(i) + 1:]
        
        return True