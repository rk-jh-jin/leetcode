# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def deleteDuplicates(self, head: ListNode) -> ListNode:
        if head == None:
            return head
        elif head.next == None:
            return head
        else:
            resultNode = None
            if head.val == head.next.val:
                resultNode = self.deleteDuplicates(head.next)
            else:
                resultNode = head
                resultNode.next = self.deleteDuplicates(head.next)
            return resultNode