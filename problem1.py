class Solution:
    def twoSum(self, nums: [int], target: int) -> [int]:
        for i in range(len(nums) - 1):
            for j in range(i + 1, len(nums)):
                if nums[i] + nums[j] == target:
                    return [i, j]

solution = Solution()
result = solution.twoSum([2, 7, 11, 19], 9)
print(result)
print([1,2] + [])