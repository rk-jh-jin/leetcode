class Solution:
    def addDigits(self, num: int) -> int:
        if num < 10:
            return num
        
        case = [int(i) for i in str(num)]
        num = sum(case)
        return self.addDigits(num)