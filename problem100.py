class Solution:
    def isPerfectSquare(self, num: int) -> bool:
        n = 1
        while n <= (num / n):
            if num / n == n:
                return True
            
            n += 1
            
        return False