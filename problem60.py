class Solution:
    def countPrimes(self, n: int) -> int:
        if n <= 2:
            return 0
        
#         count = 0
#         for i in range(1, n, 2):
#             if self.is_prime(i) is True:
#                 count += 1
        
#         return count + 1
    
#     def is_prime(self, n: int) -> bool: 
#         if n < 2: 
#             return False 
#         if n in (2, 3): 
#             return True 
#         if n % 2 is 0 or n % 3 is 0: 
#             return False 
#         if n < 9:
#             return True 
        
#         k, l = 5, n**0.5 
    
#         while k <= l: 
#             if n % k is 0 or n % (k+2) is 0: 
#                 return False 
            
#             k += 6
            
#         return True

        return len(self.primes_up_to_good(n-1))

    def primes_up_to_good(self, n: int):
        seive = [False, False] + [True] * (n - 1) 
        for k in range(2, int(n ** 0.5 + 1.5)): 
            if seive[k]: 
                seive[k*2::k] = [False] * ((n - k) // k)
                    
        return [x for x in range(n+1) if seive[x]]

