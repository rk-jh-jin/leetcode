class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        case = list(s)
        result = []
        for i in range(len(case) - 1, -1, -1):
            if case[i] == " ":
                continue
            else:
                result += [case[i]]
                if case[i - 1] == " ":
                    break
        if len(result) == 0:
            return 0
        else:
            return len(result)
        