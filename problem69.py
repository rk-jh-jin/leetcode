# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def isPalindrome(self, head: ListNode) -> bool:
        if not head:
            return True
        
        result = []
        while head != None:
            result.append(head.val)
            head = head.next
            
        result2 = result[:]
        result.reverse()
        return result2 == result
                