class Solution:
    def reverseBits(self, n: int) -> int:
        oribin='{0:032b}'.format(n)
        return int(oribin[::-1], 2)