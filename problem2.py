class Solution:
    def reverse(self, x: int) -> int:
        def check(num) -> int:
            if num < -(2 ** 31) or num > (2 ** 31) - 1:
                return 0
            else:
                return num
        
    
        newx = list(str(abs(x)))
        newx.reverse()
        result = int("".join(newx))
        result = -result if x < 0 else result
        return check(result)
            
solution = Solution()
result = solution.reverse(-123)
print(result)