# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def isSameTree(self, p: TreeNode, q: TreeNode) -> bool:
        if p == None and q == None:
            return True
        elif p == None or q == None:
            return False
        else:
            if p.val == q.val:
                if self.isSameTree(p.left, q.left) is True:
                    if self.isSameTree(p.right, q.right) is True:
                        return True
                    else:
                        return False
                else:
                    return False
            else:
                return False
            