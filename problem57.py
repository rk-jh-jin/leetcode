class Solution:
    def rob(self, nums: List[int]) -> int:
        YesRob = 0
        NoRob = 0
        for i in nums:
            NoRob, YesRob = max(NoRob, YesRob), NoRob + i
            
        return max(NoRob, YesRob)