class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        case1, case2 = list(s), list(t)
        case1.sort()
        case2.sort()
        return case1 == case2