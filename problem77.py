# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def binaryTreePaths(self, root: TreeNode) -> List[str]:
        if root == None:
            return root
        
        result = []
        string = str(root.val)
        return self.makePath(root, string, result)
        
    def makePath(self, root, string, result):
        if root.left == None and root.right == None:
            result.append(string)
        if root.left:
            result = self.makePath(root.left, string + "->" + str(root.left.val), result)
        if root.right:
            result = self.makePath(root.right, string + "->" + str(root.right.val), result)
        return result