class Solution:
    def trailingZeroes(self, n: int) -> int:
#         i = 1
#         count = 0
#         while (5 ** i) <= n:
#             count += (n // (5 ** i))
#             i += 1
            
#         return count
        
        if n < 5:
            return 0
        
        count = 0
        while n > 0:
            n = n // 5
            count += n
            
        return count