class Solution:
    def romanToInt(self, s: str) -> int:
        roman = ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I']
        numberset = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
        result = 0
        while s != "":
            substring = s[:2]
            substring2 = s[:1]
            if substring in roman:
                result += numberset[roman.index(substring)]
                s = s.replace(substring, "", 1)
            elif substring2 in roman:
                result += numberset[roman.index(substring2)]
                s = s.replace(substring2, "", 1)
        return result

